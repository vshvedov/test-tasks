$(document).ready(function() {
  $("input[name='weight_field'], :radio[name='optionsRadios']").bind("change paste keyup", function() {
      var $radio = $(":radio[name='optionsRadios']:checked");
      var $carrier_radios = $(":radio[name='optionsRadios']")
      var $weight_field = $("input[name='weight_field']");

      $carrier_radios.attr('disabled', false);

      if($radio.length == 1) {
          var carrier_data = $radio.val().split(",").map(Number);
          var weight = parseFloat($weight_field.val());
          var kilo_price = carrier_data[1] * weight

          $carrier_radios.each(function() {
            if(weight > $(this).val().split(",").map(Number)[2]) {
              $(this).attr('disabled', true);
            }
          })

          console.log('Carrier array:' + carrier_data);
          console.log('Weight:' + weight);
          console.log('Price per kilo:' + kilo_price);

          if($carrier_radios.filter(':disabled').length == 3) {
            $("#total_price").html('This parcel is over processing limits and could not be delivered.');
          } else {
            var result_price = 15 + carrier_data[0] + kilo_price
            $("#total_price").html('$' + result_price);
          }
      }
  });
});
