### Task 4

I used `npm`, `Express.js` and `Gulp.js` + `Bower` (to manage packages and build application). As prerequest you'll need `node.js >= 0.10.0`, `npm` and `bower` installed. After this:

```
npm install -g bower
cd ./task4
gulp
npm start
```

And that's it, you'll have all components installed and local server invoked on port 3000. Now you can browse to `http://localhost:3000`!