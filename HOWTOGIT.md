###Basics
Our repository is based on git, we use Github hardly for discussions and code reviews, as this is an important part of development. But **Git is not GutHub**. Sounds obvious, but you have to allways keep this in mind.

Other important part is how we maintain repository by itself. Some of ideas are partially based on [A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/) methodology, so it will be reasonable to read it.

###Repository structure is:
* `master` branch, is an stable branch, always ready for production deploys. This is a 'slow' branch, which means cummulitive merges to master take place after a few levels of reviews, frequently, before an production release, end of the sprint and so on
* `development` branch (is your friend), is an integrational branch, a bleeding edge of development. We use development branch to merge pending pull requests, update current pull request with the latest changes. It could contain a number of yet unknown bugs, which later will be discovered during reviews. You have to pull request on this branch.
* `feature/xxx` branches are features in development. Keep branches naming reasonable, starting with JIRA ticket ID (or any other project management software you use) when it's possible (e.g. **'DEV-42'**). When it's not, be creative and use 1-3 words to determine a feature name (e.g. **'improve_messges_templates'**, **'analytics'**)

**Keep git repository clean:** it means, don't forget to delete a branch after it being merged, don't create random branches for your local experiments in the upstream (use forks if you feel you need to push your branch to remote)
Commits should be atomic: one commit should contain one feature. Don't start to fix something else you found in the code, what is not connected with a feature you currently working on in the same commit: stash your current changes, fix, make a separated commit and then un-stash.

**Commit locally as frequently as you feel you need it:** commits are you local history and roadmap, commit frequently and then...
Squash similar commits: before pushing to the remote (upstream), squash commits, which contain similar actions into one commit. When you been testing something, you made a number of commits in CSS, and after this, squash and...
Keep commit description in a good shape: a first line of the commit message should be short and descriptional (>= 50 chars), after this (if you need it), it should be followed by the list of details, prepended by blank line:

```
DEV-42: messages improvements:
 
- fix CSS for templates
- introduce draper gem
- fix Message model logic
- add rabl template for messages_controller#index
- styling improvements
```

###Gitflow or not to Gitflow
You can give git-flow a try, which is a wrapper around git with **Gitflow** methodology inside. However, **this is not a mandatory 'religion' you have to be involvet to**. But if you feel frisky, first you need to setup it as a binary locally, and after this, prepare your repository:

```
git flow init -d
After this, your usual flow is:
git flow feature start DEV-42

(creates a new local 'feature/DEV-42' branch and switch to it)
... 
(here you start development)

git commit -am 'styling improvements'
...
git commit -am 'introduce draper gem'
...
git commit -am 'fix CSS for templates'
...
(then squash)
git commit -i HEAD~3
 
(now, when we done, preparing it a pull request)

git flow feature finish DEV-42
git push origin feature/DEV-42
```