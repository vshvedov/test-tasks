# There are many ways to deduplicate an array.
# I'd prefer Ruby's Array#uniq, as it's available from Ruby 1.8.x
# and works acceptable fast. However, there are other ways shown below.
#
# Benchmark results:
#
# "uniq__: [10, 1, 2, 30, -1, 100, 5, 33, -33, 85, 49, 3, 4, 48, 6, 55, 35, 93, 94, -10, -11, -42]"
# "select: [10, 1, 2, 30, -1, 100, 5, 33, -33, 85, 49, 3, 4, 48, 6, 55, 35, 93, 94, -10, -11, -42]"
# "fancy_: [10, 1, 2, 30, -1, 100, 5, 33, -33, 85, 49, 3, 4, 48, 6, 55, 35, 93, 94, -10, -11, -42]"
#  user     system      total        real
#  0.000000   0.000000   0.000000 (  0.000013)
#  0.000000   0.000000   0.000000 (  0.000031)
#  0.000000   0.000000   0.000000 (  0.000011)

require 'benchmark'

DUP_ARRAY = [
  10, 1, 2, 10, 30, -1, 2, 100, 100, 5, 33, -33, 85, 49, 3, 1, 2, 4,
  48, 4, 2, 10, 4, -1, 3, 5, 100, 4, 6, 1, 55, 35, 93, 6, 2, 1, 5, 2,
  10, 1, 2, 10, 30, -1, 2, 100, 100, 5, 33, -33, 85, 49, 3, 1, 2, 4,
  48, 4, 2, 10, 4, -1, 3, 5, 100, 4, 6, 1, 55, 35, 93, 6, 2, 1, 5, 2,
  1, 5, 5, 55, 94, 6, 100, 5, 48, 2, 100, -10, -11, -42, 100, 1, 3, 5
]

def non_duplicates_uniq(a = DUP_ARRAY)
  a.uniq!
end

def non_duplicates_select(a = DUP_ARRAY)
  counter = Hash.new(0)
  a.each { |t| counter[t] += 1 }
  counter.select { |v, c| c == 1 }.keys
end

def non_duplicates_fancy(a = DUP_ARRAY)
  result = {}
  a.each { |k| if result.has_key? k then result[k] + 1 else result[k] end }
end

p "uniq__: #{non_duplicates_uniq}"
p "select: #{non_duplicates_select}"
p "fancy_: #{non_duplicates_fancy}"

Benchmark.bm do |b|
  b.report { non_duplicates_uniq }
  b.report { non_duplicates_select }
  b.report { non_duplicates_fancy }
end

