##Introduction
Automated tests are integral part of any modern application: it allow minify support, refactoring and development expenses, leaving more resources to the new features development, add new developers into the stack faster and create robust software products.

By itself, as automated tests is nothing more than code to automate manual actions, it should consist a clean good readable code. This means that all of the good coding practices are relevant for automated tests as well, and sometimes are even more important in test code than in production code. In particular, good tests are:

* Maintainable
* Readable
* Have single responsibility (i.e. tests only a single business rule)
* Isolated
* Deterministic


###General automated testing best-practices
There are number of simple best-practices to achieve the goal, when automated tests go together with a feature development (or before it, if we are talking about TDD/BDD), helping an developer and the team do things faster than without tests:

In order to achieve both maintainability and readability of the tests, the tests must verify Business Functionality, and not rely on any implementation detail. If your tests rely on specific implementation details, then once these details are changed, your tests will break.

Good tests must describe a simple scenario from the eyes of the user (or users). A single test can describe a scenario that spans different users (roles) in different times. You have to try to avoid a situation when you test technical aspects of the state of the system, if this is not a goal of the test.
You have to make automated tests as atomic as possible, always paying attention on possible randomness in order of tests execution.

For the tests to be readable and reflect user scenario, always write your tests in a Top to the Bottom manner.
Focus on what you want to test rather then how
Try hard to avoid duplications, DRY your code as the second round of self review before the pull request. However, if you really feel DRYing gonna take a big amount of time, leave it as is and ask for help with a TODO comment

Tests should create all data or environment conditions that are relevant to it at the beginning, and clean them up afterward
As automated tests is nothing more than a code plus testing framework, it always should go through a code review
Tests should not depend on anything outside of it that may change. This includes: the order of the tests, date and time, random generator – keep in mind about random execution, use specific techniques to freeze a time and so on

###Rspec best-practices
Rspec is a great tool in the behavior-driven development, and we need to fully use it's potential, to make automated tests code reliable and maintainable. Here is the number of general guidelines for your Rspec specs:
Be clear about what method you are describing. For instance, use the Ruby documentation convention of . (or ::) when referring to a class method's name and # when referring to an instance method's name.

* Always use contexts: сontexts are a powerful method to make your tests clear and well organized. In the long term this practice will keep tests easy to read.
* Keep descriptions short and easy-readable in English
* You always have to do single-expectations specs
* Tests all possible cases, starting with edge-cases for minimum/maximum, or false/true, values (so call 0/1 testing)
* Keep guard always turned on
* Use pseudo-random data (like `faker`/`ffaker` Ruby gems) instead of lame test input like: 'a@a.com', '123123', 'John Doe'

More specific guidelines are highly connected with Rspec framework itself, they are always a point to discuss during code reviews.
