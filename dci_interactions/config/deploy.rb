lock '3.1.0'

set :application, 'someapp'
set :repo_url, 'git@github.com:some/repo.git'
set :linked_dirs, %w{ public/uploads tmp/pids log }
set :ssh_options, {forward_agent: true}
set :resque_environment_task, true

namespace :deploy do
  task :set_configs do
    on roles(:app), in: :sequence, wait: 5 do
      execute "cp #{shared_path}/config/*.yml #{release_path}/config/"
    end
  end

  task :notify_rollbar do
    on roles(:app) do |h|
      revision = `git log -n 1 --pretty=format:"%H"`
      local_user = `whoami`
      execute "curl https://api.rollbar.com/api/1/deploy/ -F access_token=#{fetch(:rollbar_token)} \
              -F environment=#{fetch(:rollbar_env)} -F revision=#{revision} \
              -F local_username=#{local_user} >/dev/null 2>&1",
              once: true
    end
  end

  task :install_js_dependencies do
    on roles(:app), in: :sequence, wait: 5 do
      execute "cd #{release_path}/ && RAILS_ENV=#{fetch(:rails_env)} ~/.rvm/bin/rvm 2.1.0@mygemset do bundle exec rake bower:install"
    end
  end

  before 'deploy:assets:precompile', 'deploy:set_configs'
  before 'deploy:assets:precompile', 'deploy:install_js_dependencies'
  after  'deploy',                   'deploy:migrate'
  after  'deploy:restart',           'resque:restart'
  after  :publishing,                :restart
end
