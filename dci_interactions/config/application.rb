require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(:default, Rails.env)

module Someapp
  class Application < Rails::Application
    config.i18n.enforce_available_locales = true

    config.autoload_paths += %W(
      #{Rails.root}/app/interactions/concerns
      #{Rails.root}/app/exceptions
    )

    config.time_zone = 'Pacific Time (US & Canada)'
  end
end
