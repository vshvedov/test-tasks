Myapp::Application.routes.draw do
  delete 'logout' => 'sessions#destroy'
  get 'login' => 'sessions#new'

  resources :passwords, only: [:create, :edit, :update]
  get 'reset_password' => 'passwords#new'
  put 'update_from_profile' => 'passwords#update_from_profile'

  resources :sessions, only: [:create]

  resources :users, only: [:update]
  get 'profile' => 'users#profile'
  get 'sign_up' => 'users#sign_up'
  put 'register' => 'users#register'
  post 'invite' => 'users#invite'

  resources :sub_accounts, only: [:index, :edit, :update]
  get 'sub_accounts/:id', to: "sub_accounts#edit"

  put 'set_current_sub_account' => 'sub_accounts#set_current'

  resources :sub_account_users, only: [:destroy]

  resources :contacts, only: [:index]
  get 'contacts/import' => 'contacts#import'

  resources :email_templates, except: [:show]

  root 'main#dashboard'
end
