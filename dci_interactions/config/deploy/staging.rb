role :app, %w{someuser@some.server.com}
role :web, %w{someuser@some.server.com}
role :db,  %w{someuser@some.server.com}
role :resque_worker, %w{someuser@some.server.com}
role :resque_scheduler, %w{someuser@some.server.com}

set :branch, 'development'
set :deploy_to, "/home/someuser/somedir"
set :rails_env, 'staging'
set :nginx_server_name, 'some.server.com'
set :unicorn_pid, "#{shared_path}/tmp/pids/unicorn.pid"

set :rollbar_token, '---'
set :rollbar_env, 'staging'
set :rvm_type, :user
set :user, 'someuser'
set :rvm_ruby_version, '2.1.0@somegemset'
set :unicorn_workers, 4

set :hipchat_token, '---'
set :hipchat_room_name, 'Engineering'
set :hipchat_announce, true

set :workers, { '*' => 1 }

server 'some.server.com', user: 'someuser', roles: %w{web app}

after 'deploy', 'deploy:notify_rollbar'
