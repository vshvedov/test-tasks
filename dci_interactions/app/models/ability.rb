class Ability
  include CanCan::Ability

  def initialize(user)
    @user = user

    can :manage, User, id: @user.id

    if user.account_owner
      can :manage, [SubAccount, SubAccountUser, Contact]
    else
      SubAccountUser::ROLES.each {|role| send(role, role)}
    end
  end

  def sub_account_manager(role)
    can :manage, SubAccount, sub_account_users: {user_id: @user.id, role: role}
    can :read, Contact, sub_account: {sub_account_users: {user_id: @user.id, role: role}}
  end

  def account_owner(role)
    can :manage, Contact, sub_account: {sub_account_users: {user_id: @user.id, role: role}}
    can :invite, SubAccount, sub_account_users: {user_id: @user.id, role: role}
    can :manage, SubAccount, sub_account_users: {user_id: @user.id, role: role}
    can :delete_user, SubAccount, sub_account_users: {user_id: @user.id, role: role}
    can :destroy, SubAccountUser, sub_account:
        {sub_account_users: {user_id: @user.id, role: role}}
  end
end