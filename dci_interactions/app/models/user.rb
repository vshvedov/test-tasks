class User < ActiveRecord::Base
  authenticates_with_sorcery!
  TIMEZONES = ActiveSupport::TimeZone.all.map(&:name)

  validates :password, length: {minimum: 6}, if: Proc.new { |user| user.password.present? }
  validates :password, presence: true, if: Proc.new { |user| user.persisted? && user.invitation_token.present? ||
                                                             user.new_record? && user.invitation_token.blank?  ||
                                                             user.reset_password_token.present? }
  validates_confirmation_of :password
  validates_presence_of :password_confirmation, if: :password
  validates :email, presence: true, uniqueness: true, format: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  validates :full_name, presence: true, if: Proc.new { |user| user.invitation_token.blank? ||
                                                              user.persisted? && user.invitation_token.present? }
  validates :last_login_from, format: /\b(?:\d{1,3}\.){3}\d{1,3}\b/, allow_blank: true
  validates_inclusion_of :time_zone, in: TIMEZONES
  validates_plausible_phone :phone

  phony_normalize :phone

  has_many :sub_account_users, dependent: :destroy
  has_many :sub_accounts, through: :sub_account_users
  belongs_to :invited_by, class_name: 'User'

  def self.invited_with_token(token)
    includes(:invited_by).where(invitation_token: token).first
  end

  def role_for_sub_account sub_account
    if sub_account_user = sub_account_users.where(sub_account: sub_account).first
      return sub_account_user.role
    end
  end

  def has_role_for_sub_account?(sub_account, role)
    role_for_sub_account(sub_account) == role
  end
end
