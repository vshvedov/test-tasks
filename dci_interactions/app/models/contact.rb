class Contact < ActiveRecord::Base
  validates :email, :source, :email_preference, :status, :sub_account_id, presence: true

  belongs_to :sub_account

  scope :not_deleted, -> { where.not(status: 'deleted') }
  scope :deleted, -> { where(status: 'deleted') }
end
