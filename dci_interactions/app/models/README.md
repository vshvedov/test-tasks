#Models

##Contact

###Attributes:

* browsers [text] - Contact browsers used. Collected from link click tracking. Can be multiple values.
* email [string, not null] - Required. Contact email address.
* email_clients [text] - Contact email clients used. Collect from email open tracking. Can be multiple values.
* email_preference [integer, default=0, not null] - Required. Contact email format preferences. Can be rather HTML or TEXT. HTML should be used by default.
 - html - 0
 - text - 1
* ip_addresses [text] - Contact IP address. Can be multiple values.
* locations [text] - Contact location, based on geoIP and last used IP address. Can be multiple values:
 - zip code,
 - city,
 - region,
 - country.
* messages_count [integer, default=0] - Messages sent to this contact. Can be huge value e.g. billion messages.
* operating_systems [text] - Contact operating system. Can be multiple values.
* orders_information [text] - Orders by contact. Loaded through API only. May have a lot of information, but at least:
 - id,
 - name,
 - category,
 - quantity,
 - revenue,
 - order date/time
* source [string, not null] - Required. Source for contact. Can be:
 - imported - 0 - imported from csv
 - api - 1 - Added by API
* status [integer, default=0, not null] - Required. Can be:
 - Active - 0 - active contact.
 - Inactive - 1 - bounced (email didn't delivered) 5 times in a row.
 - Unsubscribed - 2 - contact unsubscribed manually.
 - Deleted - 3 - deleted contact, don't show at all.
