class ContactCreationInteraction < BaseInteraction
  APPLICATION_ATTRIBUTES = [
    :email, :source, :email_preference, :status,
    :messages_count, :browsers, :email_clients,
    :operating_systems, :ip_addresses, :locations,
    :orders_information
  ]

  def initialize(sub_account_id)
    @sub_account = SubAccount.find(sub_account_id)
  rescue ActiveRecord::RecordNotFound => e
    raise InteractionValidationError.new(e)
  end

  def create(attributes)
    attributes.symbolize_keys!
    Contact.new(attributes.slice(*APPLICATION_ATTRIBUTES)).tap do |contact|
      contact.sub_account = @sub_account
      contact.locations = attributes[:locations].to_a.map(&:to_s)
      contact.orders_information = attributes[:orders_information].to_a.map(&:to_s)
      contact.save!
    end
  rescue ActiveRecord::RecordInvalid => e
    raise InteractionValidationError.new(e.record)
  end
end

