class UserInvitationInteraction < BaseInteraction

  def initialize(user_id, sub_account_id)
    @user = User.find(user_id)
    @sub_account = SubAccount.find(sub_account_id)
  rescue ActiveRecord::RecordNotFound => e
    raise InteractionValidationError.new(e)
  end

  def invite(emails, role = 'sub_account_manager', message = nil)
    raise InteractionValidationError.new('Invalid emails format!') unless valid_emails?(emails)
    emails = emails.gsub(/\s+/, '').split(',')
    emails.map do |email|
      invited_user = User.find_or_initialize_by(email: email)
      if invited_user.new_record?
        invited_user.tap do |user|
          user.invited_by_id = @user.id
          user.invitation_token = SecureRandom.urlsafe_base64
          user.crypted_password = ''
          user.salt = ''
          user.save!
        end
        UserMailer.invite_new_user(@user.id, invited_user.id, message).deliver!
      else
        if invited_user.sub_accounts.include?(@sub_account)
          raise InteractionValidationError.new('User is already a member of this sub-account!')
        else
          UserMailer.invite_existing_user(@user.id, invited_user.id, @sub_account.id, message).deliver!
        end
      end
      SubAccountUserCreationInteraction.new.create(sub_account_id: @sub_account.id, user_id: invited_user.id, role: role)
    end
  rescue ActiveRecord::RecordInvalid => e
    raise InteractionValidationError.new(e.record)
  end

  private

  def valid_emails?(emails)
    emails.to_s.match(/^([\w+-.%]+@[-\w.]+\.[A-Za-z]{2,4},?\s?)+$/)
  end
end
