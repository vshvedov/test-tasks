class UserLoggerInteraction < BaseInteraction

  def initialize(user_id)
    @user = User.find(user_id)
  rescue ActiveRecord::RecordNotFound => e
    raise InteractionValidationError.new(e)
  end

  def log(remote_ip)
    @user.update_attributes!(last_login_from: remote_ip, last_login_at: Time.zone.now)
  rescue ActiveRecord::RecordInvalid => e
    raise InteractionValidationError.new(e.record)
  end
end
