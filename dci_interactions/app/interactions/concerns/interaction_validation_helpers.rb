module InteractionValidationHelpers
  class ValidationErrorRaiser
    def error!(message)
      raise InteractionValidationError.new(message)
    end
  end

  class ValidationErrorCollector
    def initialize
      @errors = {}
    end

    def error!(message)
      if message.is_a?(Hash)
        @errors.update(message)
      else
        @errors[:base] = message
      end
    end

    def raise_errors!
      raise InteractionValidationError.new(@errors) unless @errors.empty?
    end
  end

  def validate!(condition, options = {})
    validation_error!(options[:message]) unless condition
  end

  def validate_presence!(object, options = {})
    validate!(object.present?, options)
  end

  def validate_absence!(object, options = {})
    validate!(object.blank?, options)
  end

  def validate_float!(value, options = {})
    validate!(is_float?(value), options)
  end

  def validate_date!(value, options = {})
    validate!(is_date?(value), options)
  end

  def validate_all!(&block)
    if block_given?
      begin
        @validation_handler = ValidationErrorCollector.new
        block.call
        @validation_handler.raise_errors!
      ensure
        @validation_handler = nil
      end
    end
  end

  def validate_model!(object, attributes = [])
    unless object.valid?
      messages = Hash[object.errors.to_hash.map { |attribute, errors| [attribute, errors.to_sentence] }]
      messages.slice!(*attributes) if attributes.any?
      validation_error!(messages)
    end
  end

  # @param [Hash|String] Hash of error messages or text message,
  #        i.e. {comment: 'You must specify a reason'} or "Can't create message without a subject"
  def validation_error!(message)
    validation_handler.error!(message)
  end

private

  def validation_handler
    @validation_handler ||= ValidationErrorRaiser.new
  end

  def is_float?(value)
    !!Float(value) rescue false
  end

  def is_date?(value)
    !!value.to_date rescue false
  end
end

