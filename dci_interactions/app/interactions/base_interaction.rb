class BaseInteraction
  include InteractionValidationHelpers

  # Performer is a role that performs the action.
  # If performer is nil then actions is considered as performed by system.
  attr_reader :performer

  # @param performer [Role]
  def initialize(performer = nil)
    @performer = performer
  end
end

