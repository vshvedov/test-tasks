class UserRegistrationInteraction < BaseInteraction
  attr_accessor :user
  APPLICATION_ATTRIBUTES = [
    :password_confirmation, :full_name
  ]

  def initialize(user_id)
    @user = User.find(user_id)
  rescue ActiveRecord::RecordNotFound => e
    raise InteractionValidationError.new(e)
  end

  def register(attributes)
    if @user.invitation_token.blank?
      raise InteractionValidationError.new('User is already registered!')
    else
      attributes.symbolize_keys!
      @user.tap do |user|
        user.invitation_token = ''
        user.password_confirmation = attributes[:password_confirmation]
        user.full_name = attributes[:full_name]
        user.change_password!(attributes[:password])
      end
      raise InteractionValidationError.new(@user) if @user.errors.any?
    end
  rescue ActiveRecord::RecordInvalid => e
    raise InteractionValidationError.new(e.record)
  end
end
