class BrontoContactsImport < BaseResqueJob
  include Resque::Plugins::Status
  @queue = :bronto_contacts_import

  def self.perform(sub_account_id)
    @client = Savon.client(wsdl: 'https://api.bronto.com/v4?wsdl', namespace_identifier: :v4)
    @total_contacts = 0

    for current_page in 1..ENV['BRONTO_MAX_PAGES'].to_i
      login = @client.call(:login, message: {api_token: ENV['BRONTO_TOKEN']})
      session_id = login.hash[:envelope][:body][:login_response][:return]
      all_contacts = @client.call(:read_contacts, message: {filter: nil, page_number: current_page},
                                 soap_header: {"v4:sessionHeader" => {session_id: session_id}})

      contacts = all_contacts.body[:read_contacts_response].try(:[], :return)
      if contacts && contacts.count > 0
        contacts.each do |c|
          Contact.create(sub_account_id: sub_account_id,
                         email: c[:email],
                         source: 'bronto',
                         email_preference: c[:msg_pref],
                         status: c[:status])
          @total_contacts += 1
        end
      else
        Rails.logger.info "#{@total_contacts} Bronto contacts imported"
        break
      end
    end
  end
end
