class InteractionValidationError < StandardError
  attr_reader :errors

  def initialize(message_or_model = 'Invalid')
    @errors = if message_or_model.respond_to?(:errors)
      message_or_model.errors.to_hash
    else
      wrap_message(message_or_model)
    end
    super(error_message)
  end

  # @return [String] joined error messages
  def error_message
    errors.map do |attribute, errors|
      if attribute == :base
        errors
      else
        attr_name = attribute.to_s.gsub('.', '_').humanize
        "#{attr_name} #{errors.to_sentence}"
      end
    end.flatten.join('. ')
  end

  def to_s
    @errors.inspect
  end

  private

  def wrap_message(message)
    if message.is_a?(Hash)
      Hash[message.map { |attribute, error| [attribute, [error.to_s]] }]
    else
      {base: [message.to_s]}
    end
  end
end
