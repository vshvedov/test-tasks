class UsersController < ApplicationController
  before_filter :require_login, except: [:sign_up, :register]
  before_filter :require_signed_out, only: [:sign_up, :register]

  load_and_authorize_resource :user, only: :profile

  def sign_up
    if (@user = User.invited_with_token(params[:token])) && params[:token]
      render :sign_up, layout: 'session'
    else
      redirect_to login_path, alert: 'Token is invalid!'
    end
  end

  def register
    UserRegistrationInteraction.new(params[:user][:id]).register(params[:user].to_hash)
    login(params[:user][:email], params[:user][:password])

    UserLoggerInteraction.new(params[:user][:id]).log(request.remote_ip)
    redirect_to root_path, notice: 'Logged in!'
  rescue InteractionValidationError => e
    flash[:error] = e.error_message
    redirect_to sign_up_path(token: params[:user].try(:[], :token)), layout: 'session'
  end

  def invite
    @sub_account = SubAccount.find(params[:sub_account_id])
    authorize! :invite, @sub_account
    sub_account_users = UserInvitationInteraction.new(current_user.id, params[:sub_account_id])
                                                 .invite(params[:emails], params[:role], params[:message])
    render json: {sub_account_users: sub_account_users.map{ |sub_account_user| sub_account_user.decorate.profile_hash} }
  rescue InteractionValidationError => e
    render json: {error_message: e.error_message}, status: :unprocessable_entity
  end

  def profile
  end

  def update
    UserProfileUpdatingInteraction.new(current_user.id).update(params[:user].to_hash)
    render json: {full_name: params[:user][:full_name]}
  rescue InteractionValidationError => e
    render json: {error_message: e.error_message}, status: :unprocessable_entity
  end

  private

  def require_signed_out
    redirect_to(root_path, alert: 'Please log out before accept invitation.') and return if current_user
  end
end
