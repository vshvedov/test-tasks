class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def require_sub_account
    if current_sub_account.nil?
      flash[:error] = 'Please select current Sub Account!'
      redirect_to root_path
    end
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, alert: exception.message
  end

  private

  def not_authenticated
    redirect_to login_url
  end
end
