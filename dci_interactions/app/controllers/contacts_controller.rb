class ContactsController < BaseController
  before_action :require_login
  before_action :require_sub_account

  def import
    Resque.enqueue(BrontoContactsImport, current_sub_account.id)

    redirect_to(contacts_path, notice: 'Contacts import from Bronto sucessfully started')
  end
end
