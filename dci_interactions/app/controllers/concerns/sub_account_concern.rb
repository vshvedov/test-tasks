module SubAccountConcern
  extend ActiveSupport::Concern

  def set_current_sub_account(sub_account)
    session['current_sub_account_id'] = sub_account ? sub_account.id : nil
    request.env[:current_sub_account] = sub_account
  end

  def current_sub_account_id
    session["current_sub_account_id"]
  end

  def setup_sub_account
    request.env[:current_sub_account] = if current_sub_account_id
      SubAccount.accessible_by(current_ability).where("sub_accounts.id" => current_sub_account_id).first
    else
      SubAccount.accessible_by(current_ability).first
    end

    if request.env[:current_sub_account] == nil
      session["current_sub_account_id"] = nil
    end
  end

  def current_sub_account
    setup_sub_account if request.env[:current_sub_account] == nil
    request.env[:current_sub_account]
  end
end
