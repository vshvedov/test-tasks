class SubAccountUserDecorator < Draper::Decorator
  delegate_all

  def profile_hash
    {
      id: id,
      user: user.decorate.profile
    }
  end
end
