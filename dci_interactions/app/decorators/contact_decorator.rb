class ContactDecorator < Draper::Decorator
  delegate_all

  def locations
    object.locations.map { |location| eval(location).map { |pair| pair.join(': ') }.join(', ') }.join(', ')
  end
end
