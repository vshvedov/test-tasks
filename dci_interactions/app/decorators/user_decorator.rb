class UserDecorator < Draper::Decorator
  delegate_all

  def login_timestamp
    case last_login_at
    when nil
      h.content_tag(:span, 'Never logged in', class: 'label label-default')
    else
      datetime = apply_tz(last_login_at)
      time_format ? datetime.strftime("%h %-d, %Y, %I:%M %p") : datetime.strftime("%h %-d, %Y, %H:%M")
    end
  end

  def name_or_email
    full_name.present? ? full_name : email
  end

  def profile
    {
      id: id,
      email: email,
      full_name: full_name,
      last_login_at: login_timestamp
    }
  end

  private

  def apply_tz(datetime)
    datetime.in_time_zone(time_zone) if time_zone?
  end
end
