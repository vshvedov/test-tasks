require 'resque/tasks'
require 'resque_scheduler/tasks'

task 'resque:setup' => :environment

namespace :resque do
  task :work do
    require 'resque'
    require 'resque_scheduler'
    require 'resque/scheduler'

    Resque.redis = ENV['REDIS_URL']

    Resque.schedule = YAML.load_file('./config/resque_schedule.yml')
  end
end
