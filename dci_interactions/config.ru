require 'resque/server'
require 'resque-history/server'

require ::File.expand_path('../config/environment',  __FILE__)
run Rails.application

map '/queue' do
  use Rack::Auth::Basic do |username, password|
    [username, password] == [ENV['RESQUE_USERNAME'], ENV['RESQUE_PASSWORD']]
  end
  run Resque::Server.new('/queue' => Resque::Server.new)
end
