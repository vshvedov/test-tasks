require 'rubygems'
require 'spork'

Spork.prefork do
  ENV["RAILS_ENV"] ||= 'test'
  require File.expand_path("../../config/environment", __FILE__)
  require 'rspec/rails'
  require 'rspec/autorun'
  require 'capybara/rspec'
  require 'turnip/capybara'
  require 'database_cleaner'

  Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

  ActiveRecord::Migration.check_pending! if defined?(ActiveRecord::Migration)

  RSpec.configure do |config|
    include Sorcery::TestHelpers::Rails

    config.pattern = '**/*_spec.rb'
    config.use_transactional_fixtures = true
    config.infer_base_class_for_anonymous_controllers = false
    config.order = 'random'

    config.before(:suite) do
      DatabaseCleaner.strategy = :truncation,
          {:except => %w[oauth_applications]}
      DatabaseCleaner.clean
    end
  end
end

Spork.each_run do
end
