require 'spec_helper'

class SubAccountTestController < ApplicationController
  include SubAccountConcern
  def index
    @sub_account = current_sub_account
    render nothing: true
  end

  def set_user_current_sub_account
    @sub_account = SubAccount.find(params[:id])
    set_current_sub_account(@sub_account)
    render nothing: true
  end
end

describe SubAccountTestController do

  let!(:user) { FactoryGirl.create(:user, :account_owner, :with_owner_sub_account) }
  let!(:manager) { FactoryGirl.create(:user, :with_manager_sub_account) }

  before do
    @routes.draw do
      get '/sub_account_test/index'
      put '/sub_account_test/set_user_current_sub_account'
    end
  end

  after do
    Rails.application.reload_routes!
  end

  describe 'account owner' do
    before { login_user(user) }

    it 'return current sub account' do
      sub_account = user.sub_accounts.first
      session['current_sub_account_id'] = sub_account.id
      get :index, {}
      expect(request.env[:current_sub_account]).to eq(sub_account)
    end
  end

  describe 'manager' do
    before { login_user(manager) }

    it 'returns current sub account' do
      sub_account = manager.sub_accounts.first
      session['current_sub_account_id'] = sub_account.id
      get :index, {}
      expect(request.env[:current_sub_account]).to eq(sub_account)
    end

    it 'fails to get invalid current manager sub account' do
      sub_account = SubAccount.create(name: '123')
      sub_account.sub_account_users.create(user: user, role: "manager")
      session['current_sub_account_id'] = sub_account.id
      get :index, {}
      expect(request.env[:current_sub_account]).to eq(nil)
      expect(session['current_sub_account_id']).to eq(nil)
    end

    it 'sets current manager sub account' do
      sub_account = user.sub_accounts.first
      put :set_user_current_sub_account, {id: sub_account.id}
      expect(request.env[:current_sub_account]).to eq(sub_account)
      expect(session['current_sub_account_id']).to eq(sub_account.id)
    end
  end
end
