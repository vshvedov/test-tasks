require 'spec_helper'

describe EmailTemplatesController do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:sub_account) { FactoryGirl.create(:sub_account, user: user) }
  let!(:email_templates) { FactoryGirl.create_list(:email_template, 3, sub_account: sub_account) }
  let!(:another_template) { FactoryGirl.create(:email_template) }

  describe 'logged in' do
    before { login_user(user) }

    context 'GET index' do
      describe 'with assigned sub account' do
        before(:each) { allow(controller).to receive(:current_sub_account).and_return(sub_account) }

        it 'returns http success' do
          get :index
          expect(response).to be_success
        end

        it 'assigns correct email templates' do
          get :index
          expect(assigns(:all_email_templates)).to eq(email_templates.reverse)
          expect(assigns(:all_email_templates)).to_not include(another_template)
        end
      end

      describe 'without assigned sub account' do
        before { get :index }
        its(:response) { should redirect_to(root_path) }
      end
    end

    context 'GET new' do
      describe 'with assigned sub account' do
        before(:each) { allow(controller).to receive(:current_sub_account).and_return(sub_account) }

        it 'returns http success' do
          get :new
          expect(response).to be_success
        end
      end

      describe 'without assigned sub account' do
        before { get :new }
        its(:response) { should redirect_to(root_path) }
      end
    end

    context 'POST create' do
      describe 'with assigned sub account' do
        before(:each) { allow(controller).to receive(:current_sub_account).and_return(sub_account) }

        it 'rederect to index' do
          post :create, email_template: {name: Faker::Name.name, content: 'content'}
          expect(response).to redirect_to(email_templates_path)
        end

        it 'render new' do
          post :create, email_template: {}
          expect(response).to render_template('new')
        end
      end

      describe 'without assigned sub account' do
        before { post :create }
        its(:response) { should redirect_to(root_path) }
      end
    end

    context 'GET edit' do
      let(:email_template) { FactoryGirl.create(:email_template, sub_account: sub_account) }

      describe 'with assigned sub account' do
        before(:each) { allow(controller).to receive(:current_sub_account).and_return(sub_account) }

        it 'returns http success' do
          get :edit, id: email_template.id
          expect(response).to be_success
        end
      end

      describe 'without assigned sub account' do
        before { get :edit, id: email_template.id }
        its(:response) { should redirect_to(root_path) }
      end
    end

    context 'PUT update' do
      let(:email_template) { FactoryGirl.create(:email_template, sub_account: sub_account) }

      describe 'with assigned sub account' do
        before(:each) { allow(controller).to receive(:current_sub_account).and_return(sub_account) }

        it 'redirect to index' do
          put :update, id: email_template.id, email_template: {name: Faker::Name.name}
          expect(response).to redirect_to(email_templates_path)
        end

        it 'render edit' do
          put :update, id: email_template.id, email_template: {name: nil}
          expect(response).to render_template('edit')
        end
      end

      describe 'without assigned sub account' do
        before { put :update, id: email_template.id }
        its(:response) { should redirect_to(root_path) }
      end
    end

    context 'DELETE destroy' do
      let(:email_template) { FactoryGirl.create(:email_template, sub_account: sub_account) }

      describe 'with assigned sub account' do
        before(:each) { allow(controller).to receive(:current_sub_account).and_return(sub_account) }

        it 'redirect to index' do
          delete :destroy, id: email_template.id
          expect(response).to redirect_to(email_templates_path)
        end
      end

      describe 'without assigned sub account' do
        before { delete :destroy, id: email_template.id }
        its(:response) { should redirect_to(root_path) }
      end
    end
  end

  describe 'not logged in' do
    before { logout_user }

    context 'GET index' do
      it 'not authorized' do
        get :index
        assert_redirected_to login_url
      end
    end

    context 'GET new' do
      it 'not authorized' do
        get :new
        assert_redirected_to login_url
      end
    end

    context 'POST create' do
      it 'not authorized' do
        post :create
        assert_redirected_to login_url
      end
    end

    context 'GET edit' do
      it 'not authorized' do
        get :edit, id: another_template.id
        assert_redirected_to login_url
      end
    end

    context 'PUT update' do
      it 'not authorized' do
        put :update, id: another_template.id
        assert_redirected_to login_url
      end
    end

    context 'DELETE destroy' do
      it 'not authorized' do
        delete :destroy, id: another_template.id
        assert_redirected_to login_url
      end
    end
  end
end
