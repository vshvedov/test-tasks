require 'spec_helper'

describe UsersController do
  let!(:user) { FactoryGirl.create(:user, :with_owner_sub_account) }
  context 'logged in' do
    before { login_user(user) }
    describe 'GET profile' do
      it 'returns http success on #profile' do
        get :profile
        expect(response).to be_success
      end
    end

    describe 'PUT update' do
      it 'when no errors' do
        put :update, id: user.id, user: {full_name: 'new name'}, format: :json
        expect(response.status).to eq(200)
      end

      it 'when raised error' do
        put :update, id: user.id, user: {full_name: nil}, format: :json
        expect(response.status).to eq(422)
      end
    end

    describe 'GET sign_up' do
      it 'doesn`t allow logined user to visit page' do
        get :sign_up
        assert_redirected_to root_path
      end
    end

    describe 'PUT register' do
      it 'doesn`t allow logined user to visit page' do
        put :register
        assert_redirected_to root_path
      end
    end

    describe 'POST invite' do
      it 'when no errors' do
        sub_account = user.sub_accounts.first
        UserInvitationInteraction.stub_chain(:new, :invite).and_return([FactoryGirl.create(:sub_account_user)])
        post :invite, sub_account_id: sub_account.id, format: :json
        expect(response.status).to eq(200)
      end

      it 'when raised error' do
        sub_account = user.sub_accounts.first
        allow(UserInvitationInteraction).to receive(:new).and_raise(InteractionValidationError.new('dummy'))
        post :invite, sub_account_id: sub_account.id, format: :json
        expect(response.status).to eq(422)
      end
    end
  end

  context 'not logged in' do
    before { logout_user }
    describe 'GET profile' do
      it 'not authorized' do
        get :profile
        assert_redirected_to login_url
      end
    end

    describe 'PUT update' do
      it 'not authorized' do
        put :update, id: user.id, user: {full_name: nil}, format: :json
        assert_redirected_to login_url
      end
    end

    describe 'GET sign_up' do
      let!(:invited_user) { FactoryGirl.create(:user, :invited) }

      it 'with valid invitation_token' do
        get :sign_up, token: invited_user.invitation_token
        expect(response).to render_template('sign_up')
      end

      it 'with invalid invitation_token' do
        get :sign_up, token: 'invalid token'
        assert_redirected_to login_url
        expect(flash[:alert]).to eq('Token is invalid!')
      end

      it 'with blank invitation_token' do
        get :sign_up, token: nil
        assert_redirected_to login_url
        expect(flash[:alert]).to eq('Token is invalid!')
      end
    end

    describe 'PUT register' do
      let(:invited_user) do
        user = FactoryGirl.create(:user, :invited)
      end
      let(:user_params) do
        {id: invited_user.id, email: invited_user.email, token: invited_user.invitation_token,
         full_name: Faker::Name.name, password: '111111', password_confirmation: '111111'}
      end

      it 'when no errors' do
        put :register, id: user.id, user: user_params
        assert_redirected_to root_path
        expect(flash[:notice]).to eq('Logged in!')
      end

      it 'when raised error' do
        allow(UserRegistrationInteraction).to receive(:new).and_raise(InteractionValidationError.new('dummy'))
        put :register, id: user.id, user: {}
        assert_redirected_to sign_up_path
      end
    end
  end
end
