require 'spec_helper'

describe SubAccountUsersController do
  let(:sub_account_user) { FactoryGirl.create(:sub_account_user) }
  context 'logged in' do
    describe 'DELETE destroy' do
      it 'returns status ok' do
        delete :destroy, id: sub_account_user.id
        expect(response.status).to eq(302)
      end
    end
  end

  context 'logged out' do
    describe 'DELETE destroy' do
      it 'not authorized' do
        delete :destroy, id: sub_account_user.id
        assert_redirected_to login_url
      end
    end
  end
end
