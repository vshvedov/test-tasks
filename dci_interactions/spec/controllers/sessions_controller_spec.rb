require 'spec_helper'

describe SessionsController do
  let(:user) { FactoryGirl.create(:user) }
  describe 'GET new' do
    it 'returns http success on #new' do
      get :new
      expect(response).to be_success
    end
  end

  describe 'POST create' do
    it 'when user exists' do
      post :create, email: user.email, password: '123456', remember_me: 0
      expect(response).to redirect_to(root_url)
    end

    it 'when user doesn`t exist' do
      post :create, email: Faker::Internet.email, password: '123456', remember_me: 0
      expect(response).to redirect_to(login_path)
    end

    it 'when interaction error raised' do
      allow(controller).to receive(:login).and_raise(InteractionValidationError.new('dummy'))
      post :create, email: Faker::Internet.email, password: '123456', remember_me: 0
      expect(response).to redirect_to(login_path)
      expect(flash[:alert]).to eq('dummy')
    end
  end

  describe 'DELETE destroy' do
    it 'when user is logged in' do
      login_user(user)
      delete :destroy
      assert_redirected_to login_path
    end

    it 'when user isn`t logged in' do
      delete :destroy
      assert_redirected_to login_path
    end
  end

end
