require 'spec_helper'

describe PasswordsController do
  let(:user) { FactoryGirl.create(:user) }
  describe 'GET new' do
    it 'returns http success on #new' do
      get 'new'
      expect(response).to be_success
    end
  end

  describe 'POST create' do
    it 'when email blank' do
      post :create
      expect(response).to render_template 'new'
    end

    it 'when user exists' do
      User.stub_chain(:where, :first).and_return(user)
      allow(user).to receive(:deliver_reset_password_instructions!).and_return(true)
      post :create, email: user.email
      expect(response).to redirect_to login_path
    end

    it 'when user doesn`t exist' do
      expect_any_instance_of(User).to_not receive(:deliver_reset_password_instructions!)
      post :create, email: Faker::Internet.email
      expect(response).to render_template 'new'
    end
  end

  describe 'GET edit' do
    it 'when user exists' do
      allow(User).to receive(:load_from_reset_password_token).and_return(user)
      get :edit, id: 'token'
      expect(response).to be_success
    end

    it 'when user doesn`t exist' do
      get :edit, id: 'token'
      expect(response).to redirect_to(login_url)
    end
  end

  describe 'PUT update' do
    it 'when user exists' do
      allow(User).to receive(:load_from_reset_password_token).and_return(user)
      put :update, id: user.id, user: {password: '123456', password_confirmation: '123456'}, token: 'token'
      expect(response).to redirect_to(login_url)
    end

    it 'when token incorrect' do
      put :update, id: user.id, token: 'token'
      expect(response).to redirect_to(login_url)
    end

    it 'when change_password! failed' do
      allow(User).to receive(:load_from_reset_password_token).and_return(user)
      allow(user).to receive(:change_password!).and_return(false)
      put :update, id: user.id, user: {password: '123456', password_confirmation: '123456'}, token: 'token'
      expect(response).to render_template('edit')
    end
  end

  describe 'PUT update_from_profile' do
    context 'with logined user' do
      before { login_user(user) }
      it 'updates password' do
        put :update_from_profile, id: user.id, user: {password: '123456', new_password: '1234567', new_password_confirmation: '1234567'}, format: :json
        expect(response.status).to eq(200)
      end

      it 'when current password incorrect' do
        put :update_from_profile, id: user.id, user: {password: '1111'}
        expect(response.status).to eq(422)
      end

      it 'when change_password! failed' do
        expect_any_instance_of(User).to receive(:change_password!).and_return(false)
        put :update_from_profile, id: user.id, user: {password: '123456', new_password: '1234567', new_password_confirmation: '1234567'}, format: :json
        expect(response.status).to eq(422)
      end
    end

    context 'without logined user' do
      before { logout_user }
      it 'doesn`t allow to change password' do
        put :update_from_profile, id: user.id, format: :json
        assert_redirected_to login_url
      end
    end
  end
end
