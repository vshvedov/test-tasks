require 'spec_helper'

describe ContactsController do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:sub_account) { FactoryGirl.create(:sub_account, user: user) }
  let!(:contacts) { FactoryGirl.create_list(:contact, 3, sub_account: sub_account) }
  let!(:another_contact) { FactoryGirl.create(:contact) }

  describe 'logged in' do
    before { login_user(user) }

    context 'GET index' do
      describe 'with assigned sub account' do
        before(:each) { allow(controller).to receive(:current_sub_account).and_return(sub_account) }

        it 'returns http success' do
          get :index
          expect(response).to be_success
        end

        it 'assigns correct contacts and count' do
          get :index
          expect(assigns(:contacts)).to eq(contacts)
          expect(assigns(:all_contacts_count)).to eq(contacts.count)
        end
      end

      describe 'without assigned sub account' do
        before { get :index }
        its(:response) { should redirect_to(root_path) }
      end
    end
  end

  describe 'not logged in' do
    before { logout_user }

    context 'GET index' do
      it 'not authorized' do
        get :index
        assert_redirected_to login_url
      end
    end
  end
end
