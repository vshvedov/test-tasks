require 'spec_helper'

describe SubAccountsController do

  let(:account_owner) { FactoryGirl.create(:user, :with_owner_sub_account) }
  before { login_user(account_owner) }

  let(:valid_attributes) { {name: Faker::Company.name} }
  let(:valid_session) { {} }

  describe "GET index" do
    it "get sub accounts as owner" do
      get :index, {}, valid_session
      expect(assigns(:sub_accounts).to_a).to eq(account_owner.sub_accounts.to_a)
    end
  end

  describe "GET edit" do
    it "assigns the requested sub_account as @sub_account" do
      sub_account = account_owner.sub_accounts.first
      get :edit, {:id => sub_account.to_param}, valid_session
      expect(assigns(:sub_account)).to eq(sub_account)
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested sub_account" do
        sub_account = account_owner.sub_accounts.first
        expect_any_instance_of(SubAccount).to receive(:update).with({ "name" => "new_company_name" })
        put :update, {:id => sub_account.id, :sub_account => { "name" => "new_company_name"}}, valid_session
      end

      it "assigns the requested sub_account as @sub_account" do
        sub_account = account_owner.sub_accounts.first
        put :update, {:id => sub_account.id, :sub_account => valid_attributes}, valid_session
        expect(assigns(:sub_account)).to eq(sub_account)
      end
    end

    describe "with invalid params" do
      it "assigns the sub_account as @sub_account" do
        sub_account = account_owner.sub_accounts.first
        # Trigger the behavior that occurs when invalid params are submitted
        SubAccount.any_instance.stub(:save).and_return(false)
        put :update, {:id => sub_account.id, :sub_account => {  }}, valid_session
        expect(assigns(:sub_account)).to eq(sub_account)
      end

      it "re-renders the 'edit' template" do
        sub_account = account_owner.sub_accounts.first
        # Trigger the behavior that occurs when invalid params are submitted
        SubAccount.any_instance.stub(:save).and_return(false)
        put :update, {:id => sub_account.id, :sub_account => {  }}, valid_session
        expect(response).to render_template("edit")
      end
    end
  end

end
