require 'spec_helper'

describe UserInvitationInteraction do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:sub_account) { FactoryGirl.create(:sub_account) }
  let(:interaction) { described_class.new(user.id, sub_account.id) }

  describe '#invite' do
    context 'new user' do
      context 'with valid attributes' do
        it 'creates new user' do
          expect { interaction.invite(Faker::Internet.email) }.to change{ User.count }.by(1)
        end

        it 'creates new users' do
          expect { interaction.invite([Faker::Internet.email, Faker::Internet.email].join(', ')) }.to change{ User.count }.by(2)
        end

        it 'doesn`t raise error' do
          expect { interaction.invite(Faker::Internet.email) }.to_not raise_interaction_error
        end
      end
    end

    context 'existing user' do
      let!(:existing_user) { FactoryGirl.create(:user, email: Faker::Internet.email) }

      context 'with valid attributes' do
        it 'invites user' do
          expect { interaction.invite(existing_user.email) }.to change { SubAccountUser.count }.by(1)
        end

        it 'doesn`t raise error' do
          expect { interaction.invite(existing_user.email) }.to_not raise_interaction_error
        end
      end
    end

    context 'without valid attributes' do
      it 'rises validation error' do
        expect { interaction.invite(nil) }.to raise_interaction_error
      end
    end
  end

  describe '#initialize' do
    context 'with valid attributes' do
      it 'doesn`t raise error' do
        expect { described_class.new(user.id, sub_account.id) }.to_not raise_interaction_error
      end
    end

    context 'without valid attributes' do
      it 'rises validation error' do
        expect { described_class.new(666, 777) }.to raise_interaction_error
      end
    end
  end
end
