require 'spec_helper'

describe UserRegistrationInteraction do
  let!(:user) { FactoryGirl.create(:user, :invited) }
  let(:interaction) { described_class.new(user.id) }

  describe '#register' do
    let(:default_attrs) { {full_name: 'New name', password: '123456', password_confirmation: '123456'} }

    context 'with valid attributes' do
      it 'updates user`s profile data' do
        expect { interaction.register(default_attrs) }.to change{user.reload.full_name}
      end

      it 'doesn`t raise error' do
        expect { interaction.register(default_attrs) }.to_not raise_interaction_error
      end
    end

    context 'without valid attributes' do
      let(:invalid_attrs) { {full_name: nil, password: '123', password_confirmation: '123456'} }

      it 'rises validation error' do
        expect { interaction.register(invalid_attrs) }.to raise_interaction_error
      end
    end

    context 'user already registered' do
      it 'rises validation error' do
        allow_any_instance_of(User).to receive(:invitation_token).and_return(nil)
        expect { interaction.register(default_attrs) }.to raise_interaction_error
      end
    end
  end

  describe '#initialize' do

    context 'with valid attributes' do
      it 'doesn`t raise error' do
        expect { described_class.new(user.id) }.to_not raise_interaction_error
      end
    end

    context 'without valid attributes' do
      it 'rises validation error' do
        expect { described_class.new(666) }.to raise_interaction_error
      end
    end
  end
end
