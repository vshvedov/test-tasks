require 'spec_helper'

describe ContactCreationInteraction do
  let!(:sub_account) { FactoryGirl.create(:sub_account) }
  let(:interaction) { described_class.new(sub_account.id) }

  describe '#create' do
    let(:default_attrs) { {email: Faker::Internet.email, source: 0, email_preference: 0, status: 0,
                           messages_count: 10, browsers: [], email_clients: [], operating_systems: [],
                           ip_addresses: [], locations: [], orders_information: []} }

    context 'with valid attributes' do
      it 'creates contact' do
        expect { interaction.create(default_attrs) }.to change { Contact.where(sub_account_id: sub_account.id).count }.by(1)
      end

      it 'doesn`t raise error' do
        expect { interaction.create(default_attrs) }.to_not raise_interaction_error
      end
    end

    context 'without valid attributes' do
      let(:invalid_attrs) { {email: nil, source: nil, email_preference: nil, status: nil,
                             messages_count: nil, browsers: nil, email_clients: nil,
                             operating_systems: nil, ip_addresses: nil, locations: nil, orders_information: nil} }

      it 'rises validation error' do
        expect { interaction.create(invalid_attrs) }.to raise_interaction_error
      end
    end
  end

  describe '#initialize' do
    context 'with valid attributes' do
      it 'doesn`t raise error' do
        expect { described_class.new(sub_account.id) }.to_not raise_interaction_error
      end
    end

    context 'without valid attributes' do
      it 'rises validation error' do
        expect { described_class.new(777) }.to raise_interaction_error
      end
    end
  end
end
