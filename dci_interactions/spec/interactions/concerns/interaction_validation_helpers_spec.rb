require 'spec_helper'

describe InteractionValidationHelpers do
  let(:dummy_interaction) do
    Class.new do
      include InteractionValidationHelpers
    end
  end

  subject { dummy_interaction.new }

  describe '#validate!' do
    it 'raises error if condition is false' do
      expect { subject.validate!(false) }.to raise_error(InteractionValidationError)
    end

    it 'raises error with specified message' do
      expect { subject.validate!(false, message: 'Foo') }.to raise_interaction_error.with_messages(base: ['Foo'])
    end

    it "doesn't raise error if condition is true" do
      expect { subject.validate!(true) }.not_to raise_error
    end
  end

  describe '#validation_error!' do
    it 'raises error with specified message' do
      expect { subject.validation_error!(foo: 'bar') }.to raise_interaction_error.with_messages(foo: ['bar'])
    end
  end

  describe '#validate_presence!' do
    it 'raises error if value is blank' do
      expect { subject.validate_presence!('') }.to raise_error(InteractionValidationError)
    end

    it 'raises error with specified message' do
      expect { subject.validate_presence!('', message: 'Foo') }.to raise_interaction_error.with_messages(base: ['Foo'])
    end

    it "doesn't raise error if value present" do
      expect { subject.validate_presence!('1') }.not_to raise_error
    end
  end

  describe '#validate_absence!' do
    it 'raises error if value is present' do
      expect { subject.validate_absence!('42') }.to raise_error(InteractionValidationError)
    end

    it 'raises error with specified message' do
      expect { subject.validate_absence!('42', message: {foo: 'Bar'}) }.to raise_interaction_error.with_messages(foo: ['Bar'])
    end

    it "doesn't raise error if value blank" do
      expect { subject.validate_absence!('') }.not_to raise_error
    end
  end

  describe '#validate_float!' do
    it { expect { subject.validate_float!(nil) }.to raise_interaction_error }
    it { expect { subject.validate_float!('') }.to raise_interaction_error }
    it { expect { subject.validate_float!('mew') }.to raise_interaction_error }
    it { expect { subject.validate_float!('3123 mew') }.to raise_interaction_error }

    it { expect { subject.validate_float!('  42 ') }.not_to raise_interaction_error }
    it { expect { subject.validate_float!('42') }.not_to raise_interaction_error }
    it { expect { subject.validate_float!('42.31') }.not_to raise_interaction_error }
    it { expect { subject.validate_float!(42) }.not_to raise_interaction_error }
    it { expect { subject.validate_float!(42.31) }.not_to raise_interaction_error }
  end

  describe '#validate_date!' do
    it { expect { subject.validate_date!(nil) }.to raise_interaction_error }
    it { expect { subject.validate_date!('') }.to raise_interaction_error }
    it { expect { subject.validate_date!('mew') }.to raise_interaction_error }

    it { expect { subject.validate_date!('pew Tue, 12 Nov 2013 mew') }.not_to raise_interaction_error }
    it { expect { subject.validate_date!('Tue, 12 Nov 2013') }.not_to raise_interaction_error }
    it { expect { subject.validate_date!('2013-11-12') }.not_to raise_interaction_error }
    it { expect { subject.validate_date!('2013-11-12 19:56:50 UTC') }.not_to raise_interaction_error }
    it { expect { subject.validate_date!(Time.zone.now) }.not_to raise_interaction_error }
  end

  describe '#validate_all!' do
    it 'raises all errorr at once with specified messages' do
      expect do
        subject.validate_all! do
          subject.validate!(false, message: 'Foo')
          subject.validate_absence!('')
          subject.validate_presence!('', message: {foo: 'bar'})
        end
      end.to raise_interaction_error.with_messages(base: ['Foo'], foo: ['bar'])
    end
  end

  describe '#validate_model!' do
    let(:dummy_class) do
      Class.new do
        include ActiveModel::Validations

        def self.model_name
          ActiveModel::Name.new(self, nil, 'dummy_class')
        end

        attr_accessor :foo, :bar

        validates :foo, :bar, presence: true
      end
    end

    let(:dummy_instance) { dummy_class.new }

    it 'raises error with specified message' do
      expect { subject.validate_model!(dummy_instance) }
        .to raise_interaction_error.with_messages(foo: ["can't be blank"], bar: ["can't be blank"])
    end

    context 'filter attributes errors' do
      it 'raises error with specified message' do
        expect { subject.validate_model!(dummy_instance, [:bar]) }
          .to raise_interaction_error.with_messages(bar: ["can't be blank"])
      end
    end
  end
end

