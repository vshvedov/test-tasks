require 'spec_helper'

describe UserLoggerInteraction do
  let!(:user) { FactoryGirl.create(:user) }
  let(:interaction) { described_class.new(user.id) }

  describe '#update' do
    let(:default_attrs) { Faker::Internet.ip_v4_address }

    context 'with valid attributes' do
      it 'updates user`s profile data' do
        expect { interaction.log(default_attrs) }.to change{user.reload.last_login_from}.to(default_attrs)
      end

      it 'doesn`t raise error' do
        expect { interaction.log(default_attrs) }.to_not raise_interaction_error
      end
    end

    context 'without valid attributes' do
      let(:invalid_attrs) { '123' }

      it 'rises validation error' do
        expect { interaction.log(invalid_attrs) }.to raise_interaction_error
      end
    end

    describe '#initialize' do

      context 'with valid attributes' do
        it 'doesn`t raise error' do
          expect { described_class.new(user.id) }.to_not raise_interaction_error
        end
      end

      context 'without valid attributes' do
        it 'rises validation error' do
          expect { described_class.new(666) }.to raise_interaction_error
        end
      end
    end
  end
end
