RSpec::Matchers.define :raise_interaction_error do
  chain :on_attribute do |attribute|
    @attribute = attribute
  end

  chain :with_messages do |error_messages|
    @error_messages = error_messages
  end

  match do |block|
    begin
      block.call
      false
    rescue InteractionValidationError => e
      @errors = e.errors
      result = true
      result &&= @errors.has_key?(@attribute) if @attribute
      result &&= @errors == @error_messages if @error_messages
      result
    end
  end

  description do
    expected = 'raise interaction error'
    expected << " on attribute #{@attribute}" if @attribute
    expected << " with messages #{@error_messages.inspect}" if @error_messages
    expected + if @errors
      ", but raised #{@errors.inspect}"
    else
      ", but raised none"
    end
  end

  failure_message_for_should do
    "expected to #{description}"
  end

  failure_message_for_should_not do
    "expected not to #{description}"
  end
end

