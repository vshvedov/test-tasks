require 'spec_helper'

describe ContactDecorator do
  describe '#locations' do
    it 'returns normalized string' do
      contact = ContactDecorator.decorate(FactoryGirl.create(:contact, locations: [{a: 1}.to_s, {b: 2}.to_s]))
      expect(contact.locations).to eq('a: 1, b: 2')
    end
  end

  describe '#orders_information' do
    it 'returns normalized string' do
      contact = ContactDecorator.decorate(FactoryGirl.create(:contact, orders_information: [{a: 1}.to_s, {b: 2}.to_s]))
      expect(contact.orders_information).to eq('a: 1, b: 2')
    end
  end
end
