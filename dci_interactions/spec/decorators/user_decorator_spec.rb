require 'spec_helper'

describe UserDecorator do
  let(:time) { Time.new(2002, 10, 31, 2, 2, 2, '+02:00') }

  describe '#login_timestamp' do
    let(:user_12h) { UserDecorator.decorate(FactoryGirl.create(:user, time_format: true, time_zone: 'UTC', last_login_at: time)) }
    let(:user_24h) { UserDecorator.decorate(FactoryGirl.create(:user, time_format: false, time_zone: 'UTC', last_login_at: time)) }
    let(:user_without_login) { UserDecorator.decorate(FactoryGirl.create(:user, time_format: false, time_zone: 'UTC', last_login_at: nil)) }
    let(:user_yakutsk) { UserDecorator.decorate(FactoryGirl.create(:user, time_format: false, time_zone: 'Yakutsk', last_login_at: time)) }

    it { expect(user_12h.login_timestamp).to eq('Oct 31, 2002, 12:02 AM') }
    it { expect(user_24h.login_timestamp).to eq('Oct 31, 2002, 00:02') }
    it { expect(user_yakutsk.login_timestamp).to eq('Oct 31, 2002, 09:02') }
    it { expect(user_without_login.login_timestamp).to have_css 'span', text: 'Never logged in'}
  end

  describe '#name_or_email' do
    let(:user_no_fullname) { UserDecorator.decorate(FactoryGirl.build(:user, full_name: nil)) }
    let(:user_with_fullname) { UserDecorator.decorate(FactoryGirl.build(:user)) }
    let(:user_no_email_fullname) { UserDecorator.decorate(FactoryGirl.build(:user, full_name: nil, email: nil)) }
    it { expect(user_no_fullname.name_or_email).to eq(user_no_fullname.email) }
    it { expect(user_with_fullname.name_or_email).to eq(user_with_fullname.full_name) }
    it { expect(user_no_email_fullname.name_or_email).to eq(nil) }
  end

  describe '#profile' do
    let!(:user) { FactoryGirl.create(:user, time_format: true, time_zone: 'UTC', last_login_at: time) }

    it { expect(user.decorate.profile).to eq({id: user.id,
                                               email: user.email,
                                               full_name: user.full_name,
                                               last_login_at: user.decorate.login_timestamp}) }
  end
end
