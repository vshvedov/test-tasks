FactoryGirl.define do
  factory :contact do
    association :sub_account
    email { Faker::Internet.email }
    source 0
    messages_count 10
    browsers ['Chrome 33', 'Safari 7']
    email_clients ['Mail', 'The_Bat!']
    operating_systems ['Mac OS X', 'Ubuntu']
    ip_addresses []
    locations [{ zip_code: '23851',
                 city: 'Franklin',
                 region: 'Virginia',
                 country: 'US' }.to_s,
               { zip_code: '99507',
                 city: 'Anchorage',
                 region: 'Alaska',
                 country: 'US' }.to_s ]
    orders_information [{ id: '10',
                          name: 'Order#10',
                          category: 'Bikes',
                          quantity: '1',
                          revenue: '2999',
                          order_date: (Time.now - 2.week).to_s }.to_s,
                        { id: '11',
                          name: 'Order#11',
                          category: 'Bikes',
                          quantity: '1',
                          revenue: '2999',
                          order_date: (Time.now - 1.week).to_s }.to_s ]
  end
end