FactoryGirl.define do
  factory :sub_account do
    name { Faker::Company.name }
    association :user
  end
end
