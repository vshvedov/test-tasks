FactoryGirl.define do
  factory :email_template do
    association :sub_account
    name { Faker::Name.name }
    content { Faker::HTMLIpsum.body }
  end
end