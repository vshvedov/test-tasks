FactoryGirl.define do
  factory :user do
    full_name { Faker::Name.name }
    email { Faker::Internet.email }
    password '123456'
    password_confirmation { |u| u.password }
    trait :account_owner do
      account_owner true
    end

    trait :without_password do
      password nil
      password_confirmation nil
    end

    trait :with_manager_sub_account do
      after(:create) do |u|
        sub_account = create(:sub_account)
        sub_account.sub_account_users.create(user: u, role: 'sub_account_manager')
      end
    end

    trait :with_owner_sub_account do
      after(:create) do |u|
        sub_account = create(:sub_account)
        sub_account.sub_account_users.create(user: u, role: 'account_owner')
      end
    end

    trait :invited do
      crypted_password ""
      full_name ""
      invited_by { FactoryGirl.create(:user) }
      invitation_token { SecureRandom.urlsafe_base64 }
    end
  end
end
