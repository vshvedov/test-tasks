FactoryGirl.define do
  factory :sub_account_user do
    association :sub_account
    association :user
    role 'sub_account_manager'
  end
end
