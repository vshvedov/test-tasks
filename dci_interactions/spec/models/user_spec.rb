require "spec_helper"

describe User do
  describe 'validations' do
    subject { FactoryGirl.create(:user) }

    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:full_name) }
    it { should validate_uniqueness_of(:email) }
    it { should allow_value(Faker::Internet.email).for(:email) }
    it { should_not allow_value('invalid email').for(:email) }
    it { should allow_value(Faker::Internet.ip_v4_address).for(:last_login_from) }
    it { should allow_value('').for(:last_login_from) }
    it { should_not allow_value('invalid ip').for(:last_login_from) }
    it { should ensure_inclusion_of(:time_zone).in_array(User::TIMEZONES) }
  end

  describe 'password' do
    let(:user) { FactoryGirl.create(:user) }

    it 'validates password presence on creation' do
      invalid_user = FactoryGirl.build(:user, :without_password)
      expect(invalid_user.valid?).to eq(false)
      expect(invalid_user.errors.messages.keys).to include(:password)
    end

    it 'validates password length on creation' do
      invalid_user = FactoryGirl.build(:user, password: '123')
      expect(invalid_user.valid?).to eq(false)
      expect(invalid_user.errors.messages.keys).to include(:password)
    end

    it 'validates password confirmation presence on creation' do
      invalid_user = FactoryGirl.build(:user, password: '123', password_confirmation: nil)
      expect(invalid_user.valid?).to eq(false)
      expect(invalid_user.errors.messages.keys).to include(:password_confirmation)
    end

    it 'validates password presence on reset' do
      allow(user).to receive(:reset_password_token).and_return('token')
      user.update_attributes(password: nil)
      expect(user.errors.messages.keys).to include(:password)
    end

    it 'validates password length on reset' do
      allow(user).to receive(:reset_password_token).and_return('token')
      user.update_attributes(password: '123')
      expect(user.errors.messages.keys).to include(:password)
    end

    it 'validates password confirmation on reset' do
      allow(user).to receive(:reset_password_token).and_return('token')
      user.update_attributes(full_name: 'name', password: '123', password_confirmation: nil)
      expect(user.errors.messages.keys).to include(:password_confirmation)
    end

    it 'doesn`t validate password on update' do
      user.update_attributes(full_name: 'name')
      expect(user.errors.messages.keys).to_not include(:password)
    end
  end

  describe 'sub_account' do
    let(:user) { FactoryGirl.create(:user, :with_manager_sub_account) }

    it 'has manager role' do
      sub_account = user.sub_accounts.first
      expect(user.has_role_for_sub_account?(sub_account,"sub_account_manager")).to eq(true)
    end
  end
end
