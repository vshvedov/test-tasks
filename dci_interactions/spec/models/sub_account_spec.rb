require 'spec_helper'

describe SubAccount do
  describe 'validations' do
    subject { FactoryGirl.create(:sub_account) }

    it { should validate_presence_of(:name) }
    it { should belong_to(:user) }
    it { should have_many(:sub_account_users) }
    it { should have_many(:users) }
    it { should have_many(:contacts) }
    it { should have_many(:email_templates) }
  end

  it 'has valid factory' do
    FactoryGirl.build(:sub_account).should be_valid
  end
end
