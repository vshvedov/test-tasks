require 'spec_helper'

describe SubAccountUser do
  subject { FactoryGirl.create(:sub_account_user) }

  it { should validate_presence_of(:user_id) }
  it { should validate_presence_of(:sub_account_id) }
  it { should validate_uniqueness_of(:user_id).scoped_to(:sub_account_id) }
  it { should ensure_inclusion_of(:role).in_array(SubAccountUser::ROLES) }
end
