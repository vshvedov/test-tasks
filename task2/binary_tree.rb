class BinaryTree
  def initialize(value, left, right)
    @value, @left, @right = value, left, right
  end
  attr_reader :value, :left, :right

  def self.walk(level)
    level = level.to_a
    value, left, right = level
    if value
      self.new(value, self.walk(left), self.walk(right))
    end
  end

  def tree_levels
    queue = [self]
    until queue.empty?
      node = queue.shift
      yield node
      queue << node.left if node.left
      queue << node.right if node.right
    end
  end
end

TreeNode = Struct.new(:value, :left, :right)

tree = TreeNode.new(0,
       TreeNode.new(1, nil, TreeNode.new(2, nil, nil)),
       TreeNode.new(3, TreeNode.new(4, nil, nil), TreeNode.new(5, TreeNode.new(6, nil, nil), nil)))

binary_tree = BinaryTree.walk(tree)

result = []

binary_tree.tree_levels{ |node| result << node.value }

puts result.join(' ')
