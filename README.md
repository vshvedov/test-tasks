##Task 1
###Non-duplicate Elements
**Given:** Please write function which accepts array of integers a and returns another array which consists of non duplicate elements of a, i.e. elements which appear in a only once.

**Result:** There are few ways to deduplicate an array, benchmarks are included too, please read source comments for details.

![image](http://f.cl.ly/items/062O231p0I0y3P3a420b/task1.png)

##Task 2
###Tree Levels

**Given:** Please write function tree_levels which takes one argument tree and returns its elements grouped into levels. Returned value should be an array of levels ordered from top to bottom. Each level is an array of values taken from tree nodes residing on a given tree level ordered from left to right.

**Result:** Don't have much to say here, as this is more or less classy Breadth-first search implementation

##Task 3
###Basic Data Scraping
**Given:** Please write ruby script which accepts single argument, android app package name (e.g. com.rovio.angrybirds), fetches ratings breakdown for given app and outputs it like in the following example. Extra code for error handling and arguments validation can bring bonus points for this task.

**Result:** I used [Thor](http://whatisthor.com/), wich is great CLI Ruby framework. You can install nessesary gems by `bundle` command. Here is how input handling looks like:

![image](http://f.cl.ly/items/1v2v2o140r3a1g0C3k25/Screenshot%202014-03-13%2015.37.21.png)

##Task 4
###Parcel Delivery
**Given:** The MoveFast company provides parcel delivery services. Their typical customers buy stuff on eBay asking this stuff to be delivered (usually for free) to one of MoveFast warehouses. MoveFast accumulates goods, then repacks and sends those to customers for small affordable fee. In order to provide best user experience MoveFast company would like to have online parcel delivery calculator on their website.

**Result:** By itself this task is very simple (excepts I tried to figure out why we need Width, Length and Height, and haven't found where these values could be applied), but I decided to make it as a small `Node.js`/`Express.js` based application. I used `Gulp.js` as a very fast build manager, and `bower` as assets package manager. To make it work you need `Node.js` installed. Then:

```
npm install -g bower
cd ./task4
gulp
npm start
```
And you'll have application with all prerequests up and running on `http://localhost:3000`

Splendid!

**Click to check how it works:**
[![image](http://i.geekdb.org/image/3o2e3m0a3W24/Screenshot%202014-03-14%2014.46.07.png)
](https://dl.dropboxusercontent.com/u/555632/task4/index.html)