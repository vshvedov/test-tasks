# Prerequest: Ruby >= 1.8.7
# You can do 'bundle install' to setup gems stack

require 'thor'
require 'nokogiri'
require 'open-uri'

BASE_URI = 'https://play.google.com/store/apps/details?id='
STARS = %w(5 4 3 2 1)

class GpRatingBreakdown < Thor

  desc 'parse [PACKAGE_NAME]', 'parse a package from Google Play'
  def parse(package_name)
    puts "Starting with #{package_name}"
    begin
      doc = Nokogiri::HTML(open("#{BASE_URI}#{package_name}"))
      doc.css('.rating-histogram .bar-number')
         .each_with_index
         .map{ |rating, index| puts "#{STARS[index]} stars: #{rating.text.gsub('Â ', '')} rating" }
    rescue OpenURI::HTTPError => e
      if e.message == '404 Not Found'
        puts '[!] 404 Not Found'
      else
        raise e
      end
    end
  end
end

GpRatingBreakdown.start(ARGV)
